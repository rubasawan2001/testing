import 'package:flutter/material.dart';

import 'list.dart';

class starting extends StatefulWidget {
  const starting({super.key});

  @override
  State<starting> createState() => _startingState();
}

class _startingState extends State<starting> {
  int intindex = 0;

  @override
  Widget build(BuildContext context) {
    final bodies = [
      const Center(
        child: Text('Hello From Home'),
      ),
      const Center(
        child: list(),
      ),
      const Center(
        child: Text('Hello From Settings'),
      ),  const Center(
        child: Text('Hello From Settings'),
      ),  const Center(
        child: Text('Hello From Settings'),
      ),
    ];
    return Scaffold(

      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        onTap: (index) {

          setState(() {
            intindex = index;
          });
        },
        currentIndex: intindex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.store_mall_directory,color: Colors.black),
            label: "store"
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.travel_explore_rounded,color: Colors.green),
            label: "Explore"
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart_outlined,color: Colors.black),
            label: "cart",


          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite_border,color: Colors.black,),
            label: "favorite",


          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle_rounded,color: Colors.black),
            label: "profile",
            backgroundColor: Colors.white


          ),

        ],
      ),
      body: bodies[intindex],
    );
  }
}
