

import 'package:cherry_design_admin/test/start.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class welcomeapp extends StatefulWidget {
  const welcomeapp({super.key});

  @override
  State<welcomeapp> createState() => _welcomeappState();
}

class _welcomeappState extends State<welcomeapp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [


        Container(
        height:900,
        decoration: const BoxDecoration(
          // color: new Color(0xffffff),

          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage("images/lllllll.jpg"),),
        ),),
        Positioned.fill(
          top: MediaQuery.of(context).size.height*0.2,
          child: Align(
              alignment: Alignment.center,
              child: Text("   Welcome",style: TextStyle(fontSize: 55,

                  color: Colors.white

              ),)


          ),
        ),
        Positioned.fill(
          top: MediaQuery.of(context).size.height*0.4,
          child: Align(
              alignment: Alignment.center,
              child: Text("   to our store",style: TextStyle(fontSize: 55,

                  color: Colors.white

              ),)


          ),
        ),
        Positioned.fill(
          top: MediaQuery.of(context).size.height*0.6,
          child: Align(
              alignment: Alignment.center,
              child: Text("Ger your groceries in as fast as one hour",style: TextStyle(fontSize: 20,
              color: Colors.white.withOpacity(0.6)

              ),)


          ),
        ),
        Positioned.fill(
          top: MediaQuery.of(context).size.height*0.8,
          child: Align(
              alignment: Alignment.center,
              child: _socialAuthButton("Get started")),
        )

      ],),



    );
  }

  Widget _socialAuthButton( String title) =>
      InkWell(
        onTap: (){
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => starting(),
            ),
          );
        },
        child: Padding(
          padding:  EdgeInsets.symmetric(
              horizontal:  MediaQuery.of(context).size.width*0.2,
              vertical:  MediaQuery.of(context).size.height*0.02),
          child: Container(

            child: Center(
              child: Container(
                height: MediaQuery.of(context).size.height*0.07,
                width: MediaQuery.of(context).size.width*0.6,
                decoration: const BoxDecoration(
                  color: Colors.green,

                  borderRadius: BorderRadius.all(Radius.circular(200)

                  ),
                  // image: DecorationImage(
                  //
                  //   fit: BoxFit.cover,
                  //   image: AssetImage('images/Rectangle2.jpg'),),

                ),

                alignment: Alignment.center,
                child: Text(title,
                  style: GoogleFonts.radley(
                    textStyle: const TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
}
